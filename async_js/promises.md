# Promises in JavaScript

Promises are a crucial feature in JavaScript for managing asynchronous operations and improving the readability and maintainability of code.

A promise is either resolved and return some value or it is rejected with an error. Promise takes two parameter which are functions.



**Example**

```JavaScript
const promise = (num) => new Promise((resolve,reject) => {
    if(num % 2 == 0){
        resolve("It is resolved clearly")
    } else {
        reject("Opps! it's not even")
    }
})

promise(4).then((res) => {
    console.log(res)
}).catch((err) => {
    console.error(err);
});
```

**We have some operations on Promise**
- `Promise.all()`: It returns a single Promise that resolves to an array of results from the promises passed as input.

```JavaScript
let promise3 = new Promise((res, rej) => {
    setTimeout(() => {
        res(6);
    }, 3000);
});

let promise1 = new Promise((resolve, reject) => {
    setTimeout(() => resolve(2), 1000);
});

let promise2 = new Promise((resolve, reject) => {
    setTimeout(() => resolve(1), 2000);
});


console.log("calling all the promises");
Promise.all([promise1, promise2, promise3]).then((fromRes) => {
    console.log("done playing all");
    console.log(fromRes);
});


// This will print:

// calling all the promises
// done playing all
// [2, 1, 6]
```

- `Promise.race()` : It returns a single Promise that resolves first among the all promises.

```JavaScript
let promise3 = new Promise((res, rej) => {
    setTimeout(() => {
        res(6);
    }, 3000);
});

let promise1 = new Promise((resolve, reject) => {
    setTimeout(() => resolve(2), 1000);
});

let promise2 = new Promise((resolve, reject) => {
    setTimeout(() => resolve(1), 2000);
});


console.log("calling all the promises");
Promise.race([promise1, promise2, promise3]).then((fromRes) => {
    console.log("done playing all");
    console.log(fromRes);
});

// This will Print: 

// calling all the promises
// done playing all
// 2
```
This print 2 as Promise1 is resolved first so it passes the value to the fromRes and that is being printed.