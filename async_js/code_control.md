# Code Control

In JavaScript, code execution happens ina sequential manner that means it executes line by line (syncronously in a single thread),but code controle is managing the flow of asyncromous operations with the help of callbacks and promises.

```JavaScript
function helloAfter2Sec(){
    setTimeout(() => {
        console.log("Hello");
    }, 2000);
}
console.log("1")
helloAfter2Sec();
console.log("2")

//This will print: 

// 1
// 2
// Hello
```

So, as we can se that the code is skipping this function and moving to the next console without waiting for the settimeout.

