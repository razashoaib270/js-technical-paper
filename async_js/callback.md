# Callbacks in JavaScript

In JavaScript, functions are objects and since we can pass objects to a function that means we can also pass function to a function, and passing function to a function as a parameter and calling it inside the functon is called callback.

```JavaScript
function sayHello(callback) {
    callback();
}

sayHello(() => {
    console.log('hello');
});
```

Callback are generally used when we want to run a function after something or some event is fired by the user like clinking a button.

The best way to write a callback is error first callback where we handle the error first.

```JavaScript
const fs = require("fs");

function readFile(filename, callback){
   const fileExists = fs.existsSync(filename);
   if (fileExists == true) {
    callback(null, "This is some data from the file.");
   } else {
     callback('Error reading file', null);
   }
};

readFile("test.txt", (err, data)=>{
    if(err){
        return console.log(`ERROR: ${err}`);
    }else{
       console.log(data);
    }
});
```
## Callback Hell / Pyramid of Doom

It is a situation when we have Callback inside a callback inside a callback and our code starts shifting towards the right directions leaving our code almost unreadable and hard to maintain. 

```JavaScript
fetchData("url1", function (data1) {
    process(data1, function (processedData1) {
        fetchData("url2", function (data2) {
            process(data2, function (processedData2) {
                fetchData("url3", function (data3) {
                    process(data3, function (processedData3) {
                        fetchData("url4", function (data4) {
                            process(data4, function (processedData4) {
                                // ... and so on
                            });
                        });
                    });
                });
            });
        });
    });
});
```

However, we can avoid this situation by using the chained promis