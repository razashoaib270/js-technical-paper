# For Loop in JavaScript

As in other programming languages, for loop in javascript also iterates though a list of object, array i.e. any iterable variable.

#### SYNTAX
```
for(variable initialization; break condition; variable updation){
    //code execution over the itteration
   // print statements etc.....
}
```

#### EXAMPLE
```javascript
const array = [1,2,4,6,8,3,5,6];
for(let i = 0; i < array.length; i++ ){
    console.log(`The value at index ${i} is ${array[i]}`);
}
```