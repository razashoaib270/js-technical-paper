# ForEach Loop in JavaScript

ForEach loop iterates though an array but it doesn't returns something.
It returns `undefined`

It takes one function as an argument and that function takes three optional argument i.e `currentValue`, `index`, `arr`

#### EXAMPLE 
```javascript
const array = [1,2,3,5,4,7,8,9]
array.forEach((item) => console.log(item)) 
```